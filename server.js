var express = require('express');
const api = require('./api');
const dotenv=require("dotenv").config();
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
corsOptions={
    "Access-Control-Request-Method": "POST,GET,PUT,DELTE,OPTIONS",
    "Access-Control-Request-Headers": "origin",
    "Origin": "*"
}

app.use(cors(corsOptions));

// support parsing of application/json type post data
app.use(bodyParser.json());

//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api',api);


app.get('/',(req,res)=>{
    res.send("hello world");
})


const port = process.env.port || 4000;

app.set('port',port);

app.listen(app.get('port'),function(){
    console.log('your app listening on port no ' + app.get('port'))
})