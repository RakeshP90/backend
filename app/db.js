const Sequelize = require('sequelize');

const connection = new Sequelize('myapp','root','root',{
    host:'localhost',
    dialect:'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      },
});

var User = connection.define('user', {
  UserName: {
      type: Sequelize.STRING,
      default:null
  },
    email: {
    type: Sequelize.STRING,
    default:null
  },   
  password: {
      type: Sequelize.STRING,
      default:null
  },
  authToken: {
    type: Sequelize.STRING,
    default:null
  },
  deletedAt: {
    type: Sequelize.DATE,
    default:Sequelize.NOW
  }
  });

  var forgetpassword=connection.define('forgetpassword',
  {
    
  })

  var OTP=connection.define('OTP',
  {
  UserId:
    {
      type: Sequelize.BIGINT,
      default:null
    },
    GeneratedOtp:
    {
      type: Sequelize.STRING,
      default:null 
    },
    deletedAt: {
      type: Sequelize.DATE,
      default:Sequelize.NOW
    }
  })

  var Dropdown=connection.define('dropdown',
  {
    dropdownName:
    {
      type: Sequelize.STRING,
      default:null
    },
    deletedAt: {
      type: Sequelize.DATE,
      default:Sequelize.NOW
    }
  })

  var MultiDropdown=connection.define('multidropdown',
  {
    multiseldropName:
    {
      type: Sequelize.STRING,
      default:null
    },
    deletedAt: {
      type: Sequelize.DATE,
      default:Sequelize.NOW
    }
  })

  
  connection.sync({
  });

  exports.User = User;
  exports.Dropdown=Dropdown;
  exports.MultiDropdown=MultiDropdown;
  exports.OTP=OTP;
