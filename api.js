const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt=require('bcrypt');
const router = express.Router();
const db = require('./app/db')
const nodemailer = require("nodemailer");
const rn = require('random-number');

// router.get('/',(req,res) => {
//     res.send('hello app working')
// });



const gen = rn.generator({
  min:  524201
  , max:  835368
  , integer: true
})

var transporter = nodemailer.createTransport({
  service: 'gmail',
  port:3000,
  host:"http://localhost:3000",
  secure:false,
  auth: {
      user: "rakesh.padmanna@gmail.com",
      pass: "padmareddy"
  },
  tls:{
      rejectUnauthorized:false
  }
});




router.post('/users/authenticate',(req,res) => {
  //res.send({"message":"User Logged in successfully!"})
    // const userName = req.body.username;
    
    // const password = req.body.password;

    db.User.findOne({where:{UserName : req.body.username}}).then(user => {
      console.log(JSON.stringify(user));
      

          if (!user) {
            return res.status(401).send({
              message:
                "Auth failed!! either the account does't exist or you entered a wrong account"
            });
          }
          const isValid = bcrypt.compareSync(req.body.password, user.password);
            if (isValid) {
              console.log("token generating-----------")
              const token = jwt.sign(
                {
                  id: user.id
                },
                process.env.JWT_KEY,
                {
                  expiresIn: "300"
                }
              );
    
              db.User.update({ authToken: token }, {
                  where: {
                      id: user.id
                  }
              }).then((tokenUpdated) => {
                if(tokenUpdated){
                  db.User.findOne({where:{id:user.id}}).then(userResponse =>{
                      res.send({"message":"User Logged in Successfully!","status":200,"userData":userResponse});
                  })
                }  
              })
            }
           
          });
        })
    //     if(currentUser)
    //     {
    //     // const payload = { user: currentUser.name };
    //     // const options = { expiresIn: '2d' };
    //     const secret = process.env.JWT_SECRET;
    //     const token = jwt.sign(secret);

    //     console.log('TOKEN', token);
    //     result.token = token;
    //     result.status = status;
    //     result.result = user;
    //   } else If
    //   {
    //     status = 401;
    //     result.status = status;
    //     result.error = `Authentication error`;
    //   }
    //     res.send({"status":200,"message" : "User LoggedIn Successsfully!","userData":JSON.stringify(currentUser)})
    //     // else
    //     // {
    //     //     res.send({"status":401,"message":"User not existed!","userData":"null"});
    //     // }
        

// })

router.post('/users/register',(req,res) => {
  console.log(req.body)
  db.User.findOne({where:{UserName : req.body.UserName}}).then(user=>
    {
      if(user)
      {
        res.send({"user":null,"status":401,message:"user already exists!!!"})
      }
      else{
        db.User.create({
          UserName : req.body.UserName,
          email : req.body.email,
          password : bcrypt.hashSync(req.body.password,10)
      }).then(user => {
          if(user )
          res.send({"user":user,"status":200,message:"User Registred Successfully !"})
          else
          res.send({"user":null,message:"User not created!"})
      },error => {
          res.send(error)
      })
      }
    })
    
    

})



router.get('/users',(req,res) => {
db.User.findAll().then(allUsers => {
  res.send({"status":200,users:allUsers})
})
  

})


router.post('/forgetpassword',(req,res)=>
{
  db.User.findOne({where:{email:req.body.email}}).then(userobj=>
    {
      const randomOtp = gen()
      if(userobj)
      {
       console.log();
       
var mailOptions = {
from: 'rakesh.padmanna@gmail.com',
to: req.body.email,
subject: 'Ledgure Reset Password',
text: 'Hello' + userobj.name + ' !,Otp for your password reset is:' + randomOtp
};

transporter.sendMail(mailOptions, function(error, info){
  if (error) {
      res.send({"status":500,"message":"Something went wrong with mail sending !"})
  } else {


    console.log("email sent to "+ info.response);
    
     // saveOtp(userExisted,res,randomOtp)
  }
  });

}
else
res.send({"status":400,message:"Please Enter your valid email id"})

      
    })


})

router.post('/addDropdown',(req,res)=>
{

  // creating multipe record
  count=0;
  const arr=req.body.dropdownname;
  arr.map(obj =>{
    db.Dropdown.findOne({where:{dropdownName:obj.name}}).then(names=>
      {
        if(!names)
        {
          db.Dropdown.create({
            dropdownName:obj.name
          }).then(dropdown=>
            {
              count++;
              if(count == arr.length)
              res.send({status:200,message:'Drop down added successfully!'})
            })
        }
        else{
          res.send({status:204,message:"Drop down name already !"})
        }
      })
  });
    
  


  // CREATING ONE RECORD 

  // db.Dropdown.findOne({where:{dropdownName:req.body.dropdownname}}).then(names=>
  //   {
  //     if(!names)
  //     {
  //       db.Dropdown.create({ 
  //         dropdownName:req.body.dropdownname
  //       }).then(dropdown =>{
  //         if(dropdown)
  //         res.send({status:200,message:'Drop down added successfully!'})
  //       })
  //     }else{
  //       res.send({status:204,message:'Drop down name already !'})
  //     }
  //   })
})

router.post('/addmultiselect',(req,res)=>
{
  console.log("====response of all user==============");
  console.log(res);
  
  count=0;
  const arr=req.body.multiselectname;
  arr.map(obj =>{
    db.MultiDropdown.findOne({where:{multiseldropName:obj.name}}).then(names=>
      {
        if(!names)
        {
          db.MultiDropdown.create({
            multiseldropName:obj.name
          }).then(dropdown=>
            {
              count++;
              if(count == arr.length)
              res.send({status:200,message:'Drop down added successfully!'})
            })
        }
        else{
          res.send({status:204,message:"Drop down name already !"})
        }
      })
  });
})

router.get('/usersdropdown',(req,res) => {
  console.log("====response of all user==============");
  
  console.log(res);
  
  db.Dropdown.findAll().then(allUsers => {
    res.send({"status":200,"users":allUsers})
  })
})

router.get('/multiselect',(req,res) => {
  
  
  db.MultiDropdown.findAll().then(allUsers => {
    res.send({"status":200,"multiprooflist":allUsers})
  })
})
module.exports = router;